import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class InterfacesGraficas extends JFrame implements ActionListener {

	// Botões
	// ------
	/* A classe JButton é usada para trabalhar com botões. 
	 * Dentre os métodos mais utilizados da classe JButton estão:
	 * - JButton(): construtor que cria um botão sem texto.
	 * - JButton( String ): construtor que cria um botão exibindo o texto 
	 * passado como parâmetro.
	 * - JButton(String, Image): construtor que cria um botão com texto e imagem.
	 * - getLabel(): obtém o texto apresentado pelo botão.
	 * - setLabel(String): define o texto a ser apresentado pelo botão.
	 * - setEnabled(boolean): define se o botão está habilitado (true) 
	 * ou desabilitado (false).
	 * - setHorizontalTextPosition(): define o alinhamento horizontal, que pode
	 * ser: LEFT (esquerda) ou RIGHT (direita ).
	 * - setVerticalTextPosition(): define o alinhamento vertical que pode ser:
	 * TOP (por cima) ou BOTTOM (por baixo).
	 * - setMnemonic(int): define o atalho (combinação de teclas) para acionar 
	 * o botão (equivalente ao clique sobre o botão).
	 * - setToolTipText(String): possibilita colocar uma mensagem de ajuda no botão.
	 */
	private JButton b1, b2, b3;
	
	// Labels
	// ------
	/* A classe JLabel é utilizada para criar labels de textos nas janelas.
	 * A classe JLabel fornece vários construtores dentre dentre os quais estão:
	 * - JLabel(): construtor padrão.
	 * - JLabel(String): recebe como parâmetro uma String que será o texto 
	 * apresentado pelo Label.
	 * - JLabel(String, int): além do texto a ser apresentado, recebe como 
	 * parâmetro um inteiro que representa o tipo de alinhamento a ser utilizado.
	 * - JLabel(String, Image): além do texto a ser apresentado, recebe como 
	 * parâmetro um image que será o ícone a ser exibido.
	 * - JLabel(String, Image, int ): recebe como parâmetros o texto a ser 
	 * apresentado, o ícone a ser exibido e o tipo de alinhamento a ser utilizado.
	 */
	private JLabel label1, label2;
	
	/* A classe JTextField é utilizada para criar caixas de texto.
	 * Dentre os métodos utilizados para manipular um JtextField, destacam-se:
	 * - JTextField(): construtor padrão utilizado para criar uma caixa de 
	 * texto vazia.
	 * - JTextField(String): construtor utilizado para criar uma caixa de 
	 * texto com a String fornecida.
	 * - JTextField(int): construtor utilizado para criar uma caixa de texto 
	 * com a quantidade de colunas especificada.
	 * - JTextField(String, int): construtor utilizado para criar uma caixa 
	 * de texto com uma determinada String e com a quantidade de colunas 
	 * especificada.
	 * - getText(): obtém o texto do objeto.
	 * - setText(String): atribui uma String ao objeto.
	 * - getSelectedText(): obtém o texto selecionado no objeto.
	 * - isEditable(): verifica se o componente é editável ou não e retornando 
	 * um boolean (true ou false).
	 * - setEditable( boolean ): especifica se o componente é editável ou não.
	 */
	private JTextField text1;
	
	// Construtor da classe
	public InterfacesGraficas() {
		// Instanciando o novo botão
		b1 = new JButton("Mensagem");
		// Alinhamento horizontal do texto
		b1.setHorizontalTextPosition(AbstractButton.RIGHT);
		// Cor do botão
		b1.setBackground(new Color(255, 255, 255));
		// Cor do texto do botão
		b1.setForeground(Color.black);
		// Fonte do texto do botão
		b1.setFont(new Font("Scripts", Font.BOLD, 20));
		// Habilita o botão para poder ser clicado
		b1.setEnabled(true);
		// Habilita a sugestão quando o cursor é colocado sobre o botão
		b1.setToolTipText("Clique aqui para ver a mensagem");
		// Habilita tecla de atalho para uso do botão (Alt + B)
		b1.setMnemonic(KeyEvent.VK_B);
		// Adiciona o botão ao ActionListener
		b1.addActionListener(this);

		// Instanciando o novo botão
		b2 = new JButton("Login");
		// Alinhamento horizontal do texto
		b2.setHorizontalTextPosition(AbstractButton.RIGHT);
		// Cor do botão
		b2.setBackground(new Color(255, 255, 255));
		// Cor do texto do botão
		b2.setForeground(Color.black);
		// Fonte do texto do botão
		b2.setFont(new Font("Scripts", Font.BOLD, 20));
		// Habilita o botão para poder ser clicado
		b2.setEnabled(true);
		// Habilita a sugestão quando o cursor é colocado sobre o botão
		b2.setToolTipText("Clique aqui para fazer o login");
		// Habilita tecla de atalho para uso do botão (Alt + C)
		b2.setMnemonic(KeyEvent.VK_C);
		// Adiciona o botão ao ActionListener
		b2.addActionListener(this);

		// Instanciando o novo botão
		b3 = new JButton("Texto");
		// Alinhamento horizontal do texto
		b3.setHorizontalTextPosition(AbstractButton.RIGHT);
		// Cor do botão
		b3.setBackground(new Color(255, 255, 255));
		// Cor do texto do botão
		b3.setForeground(Color.black);
		// Fonte do texto do botão
		b3.setFont(new Font("Scripts", Font.BOLD, 20));
		// Habilita o botão para poder ser clicado
		b3.setEnabled(true);
		// Habilita a sugestão quando o cursor é colocado sobre o botão
		b3.setToolTipText("Clique aqui para mostrar o conteúdo do campo texto");
		// Habilita tecla de atalho para uso do botão (Alt + C)
		b3.setMnemonic(KeyEvent.VK_D);
		// Adiciona o botão ao ActionListener
		b3.addActionListener(this);

		// Define o primeiro label
		this.label1 = new JLabel("Esquerda", JLabel.LEFT);
		// Define o segundo label
		this.label2 = new JLabel("Direita", JLabel.RIGHT);
		
		// Cria novo campo de texto
		this.text1 = new JTextField(30);
		
		// Janelas
		// -------
		/* A classe JFrame provê um conjunto de métodos que permitem criar 
		 * e configurar janelas. Abaixo são citados alguns deles:
		 * - JFrame(): construtor padrão. Apenas cria uma nova janela.
		 * - JFrame(String t): cria uma janela atribuindo um título a mesma.
		 * - getTitle(): obtém o título da janela.
		 * - setTitle( String t ): atribui um título à janela.
		 * - isResizable(): verifica se a janela é redimensionável.
		 * - setResizable(boolean b): especifica se a janela é ou não 
		 * redimensionável.
		 * - setSize(int l, int a): define o tamanho da janela. Os parâmetros 
		 * passados são a largura e a altura da janela.
		 * - setLocation(int x, int y): define a posição da janela na tela.
		 */
		// Coloca o título da janela
		this.setTitle("Insering botões na janela");
		// Define o tamanho da janela
		this.setSize(350,200);
		// Define a localização na tela que a janela será aberta
		this.setLocation(50,50);
		// Define o fundo da janela
		this.getContentPane().setBackground(new Color(0, 0, 255));
		// Gerenciador de layout
		this.getContentPane().setLayout(new FlowLayout());
		//this.getContentPane().setLayout(new GridLayout(2,1));
		// Adiciona o primeiro label à janela
		this.getContentPane().add(this.label1);
		// Adiciona o primeir botão à janela
		this.getContentPane().add(this.b1);
		// Adiciona o segundo label à janela
		this.getContentPane().add(this.label2);
		// Adiciona o segundo botão à janela
		this.getContentPane().add(this.b2);
		// Adiciona campo de texto à janela
		this.getContentPane().add(this.text1);
		// Adiciona o segundo botão à janela
		this.getContentPane().add(this.b3);
	}
	
	// Trata as ações dos botões
	public void actionPerformed(ActionEvent e) {
		// Caso ação venha do botão 1
		if (e.getSource() == b1)
			// Exibe mensagem simples
			JOptionPane.showMessageDialog(null, "Mensagem", "Botão clicado",
					JOptionPane.INFORMATION_MESSAGE);
		// Caso ação venha do botão 2
		if (e.getSource() == b2) {
			// Exibe InputDialog
			String s = JOptionPane.showInputDialog(null, "Digite seu login",
					"Login no sistema", JOptionPane.QUESTION_MESSAGE);
			// Se a entrada for vazia, retorna
			if (s == null) return;
			// Se houver entrada
			if (JOptionPane.showConfirmDialog(null, "Confirma Login?",
					"Confirmação", JOptionPane.OK_CANCEL_OPTION,
					JOptionPane.QUESTION_MESSAGE) == 0)
				// Mostra uma mensagem com o login
				JOptionPane.showMessageDialog(null, s, "Login Confirmado",
						JOptionPane.INFORMATION_MESSAGE);
		}
		// Caso a ação venha do botão 3
		if (e.getSource() == b3) {
			// Exibe mensagem com o texto do campo
			JOptionPane.showMessageDialog(null, this.text1.getText(), 
					"Texto do campo", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	public static void main(String[] args) {
		// Instancia a janela
		JFrame janela = new InterfacesGraficas();
		//janela.setUndecorated(true);
		//janela.getRootPane().setWindowDecorationStyle(JRootPane.FRAME);
		// Define a operação padrão para quando fechar a janela
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Habilita a janela
		janela.setVisible(true);
	}
}
