/*
 * Dicas SQL:
 * 
 * Criar um novo usuário:
 * CREATE USER 'anhanguera'@'%' IDENTIFIED BY 'anhanguera';
 * 
 * Dar privilégio total ao usuário:
 * GRANT ALL PRIVILEGES ON *.* TO 'anhanguera'@'%' WITH GRANT OPTION;
 * 
 * Carregar arquivo sql no banco:
 * source tables.sql
 * 
 * Adicionar o Connector MySQL ao projeto:
 * Clique com o botão direito do mouse sobre o projeto e vá em 'Properties';
 * Vá na opção 'Java Build Path' e na aba 'Libraries';
 * Clique em 'Add external JARS...' e selecione o conector MySQL;
 * Clique em 'OK'.
 */


/*
JDBC
----

	JDBC é uma API para a linguagem de programação Java que define como 
um cliente pode acessar um banco de dados. Ele fornece métodos para
consultar e atualizar dados em um banco de dados. O JDBC é orientado
para bancos de dados relacionais. Do ponto de vista técnico, a API é
como um conjunto de classes no pacote java.sql. Para usar o JDBC com
um banco de dados específico, precisamos de um driver JDBC para esse
banco de dados.

	JDBC é uma pedra angular para a programação de banco de dados em Java.
Hoje, é considerado como uma programação de muito baixo nível e 
propenso a erros. Soluções como MyBatis ou JdbcTemplate foram criadas
para aliviar a carga da programação JDBC. No entanto, por trás, essas
soluções ainda usam JDBC. O JDBC faz parte da plataforma Java Standard
Edition.

	A JDBC gerencia estas três principais atividades de programação:

	- Conexão a um banco de dados;
	- Envio de consultas e instruções de atualização para o banco de 
dados;
	- Recuperar e processar os resultados recebidos do banco de dados 
em resposta à consulta.

MySQL Connector/J
----- -----------

	Para se conectar ao MySQL em Java, o MySQL fornece o MySQL 
Connector/J, um driver que implementa a API JDBC. O MySQL Connector/J 
é um driver JDBC Tipo 4. A designação Tipo 4 significa que o driver 
é uma implementação Java pura do protocolo MySQL e não confia nas 
bibliotecas do cliente MySQL.

Sobre banco de dados MySQL
----- ----- -- ----- -----

	O MySQL é um sistema líder de gerenciamento de banco de dados de código 
aberto. Trata-se de um sistema de gerenciamento de banco de dados 
multi-usuário multithread. O MySQL é especialmente popular na web. É 
uma parte da plataforma LAMP muito popular que consiste em Linux, Apache, 
MySQL e PHP. Atualmente, o MySQL é propriedade da Oracle. O banco de dados 
MySQL está disponível nas plataformas mais importantes do sistema 
operacional. Ele é executado no BSD Unix, Linux, Windows ou Mac OS. 
Wikipedia e YouTube usam MySQL. Esses sites gerenciam milhões de consultas 
por dia. O MySQL vem em duas versões: sistema servidor MySQL e sistema 
embutido MySQL.

 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Version {

    public static void main(String[] args) {

        Connection con = null;
        Statement st = null;
        ResultSet rs = null;

        /*
        String de conexão
		------ -- -------

			Uma conexão de banco de dados é definida com uma string de conexão. 
		Ela contém informações como tipo de banco de dados, nome do banco de 
		dados, nome do servidor e número da porta. Ela também pode conter pares 
		chave/valor adicionais para configuração. Cada banco de dados tem seu 
		próprio formato de string de conexão.

		À seguir temos um exemplo de uma string de conexão MySQL:

		jdbc:mysql://[host1][:port1][,[host2][: port2]]...[/[banco de dados]]
    	[?Propriedade1=Valor1[&Propriedade2=Valor]...]

		É possível especificar vários hosts para uma configuração de failover 
		do servidor. Os itens entre colchetes são opcionais. Se nenhum host 
		for especificado, o nome do host será padronizado para localhost. Se 
		a porta para um host não for especificada, o padrão é 3306, o número 
		de porta padrão para servidores MySQL.

		jdbc:mysql://localhost:3306/testdb?UseSSL=false
		
		Este é um exemplo de uma sequência de conexão MySQL. O jdbc:mysql:// 
		é conhecido como um sub-protocolo e é constante para o MySQL. Nós 
		nos conectamos ao localhost na porta padrão MySQL 3306. O nome do 
		banco de dados é testdb. Os pares de chave/valor adicionais seguem 
		o caractere de ponto de interrogação (?). O useSSL=false diz ao 
		MySQL que não haverá nenhuma conexão segura.
  
        Neste exemplo nós nos conectamos ao banco de dados e recebemos algumas 
        informações sobre o servidor MySQL.

        Esta é a URL de conexão do banco de dados MySQL. Cada driver tem uma 
        sintaxe diferente para a URL. No nosso caso, fornecemos um host, uma 
        porta e um nome de banco de dados.
         */
        //String url = "jdbc:mysql://localhost:3306/testdb";
        String url = "jdbc:mysql://192.168.122.51:3306/testdb?autoReconnect=true&useSSL=false";
        String user = "anhanguera";
        String password = "anhanguera";

        try {
            
        	/*
        	Estabelecemos uma conexão com o banco de dados, usando a URL de 
        	conexão, nome de usuário e senha. A conexão é estabelecida com 
        	o método getConnection ().
        	 */
            con = DriverManager.getConnection(url, user, password);
            
            /*
            O método createStatement() do objeto de conexão cria um objeto 
            Statement para enviar instruções SQL para o banco de dados.
             */
            st = con.createStatement();
            
            /*
            O método executeQuery() do objeto de conexão executa a instrução 
            SQL fornecida, que retorna um único objeto ResultSet. O ResultSet 
            é uma tabela de dados retornados por uma instrução SQL específica.
             */
            rs = st.executeQuery("SELECT VERSION()");

            /*
            Um objeto ResultSet mantém um cursor apontando para sua linha 
            atual de dados. Inicialmente o cursor é posicionado antes da 
            primeira linha. O método next() move o cursor para a próxima 
            linha. Se não houver linhas restantes, o método retornará false. 
            O método getString() recupera o valor de uma coluna especificada. 
            A primeira coluna tem índice 1.
             */
            if (rs.next()) {
                System.out.println(rs.getString(1));
            }

        /*
        Em caso de uma excepção, registramos a mensagem de erro. Para este 
        exemplo de console, a mensagem é exibida no terminal.
         */
        } catch (SQLException ex) {
        
            Logger lgr = Logger.getLogger(Version.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        /*
        Dentro do bloco finally, fechamos os recursos do banco de dados. 
        Também verificamos se os objetos não são iguais a null. Isso é para 
        evitar exceções de ponteiro nulo. Caso contrário, poderemos obter um 
        NullPointerException, que encerraria o aplicativo e deixaria os 
        recursos não limpos.
         */
        } finally {
            
            try {
                
                if (rs != null) {
                    rs.close();
                }
                
                if (st != null) {
                    st.close();
                }
                
                if (con != null) {
                    con.close();
                }

            /*
            Registramos uma mensagem de erro quando os recursos não 
            puderam ser fechados.
             */
            } catch (SQLException ex) {
                
                Logger lgr = Logger.getLogger(Version.class.getName());
                lgr.log(Level.WARNING, ex.getMessage(), ex);
            }
        }
    }
}
