
public class NumeroNegativoException extends RuntimeException{
/*
 * Podemos transformar essa Exception de unchecked para checked, 
 * obrigando a quem chama esse método a usar try-catch, ou throws.
 */
//public class NumeroNegativoException extends Exception{
	NumeroNegativoException(String message) {
        super(message);
	}
}
