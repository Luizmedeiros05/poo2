// http://docstore.mik.ua/orelly/java-ent/jnut/ch12_01.htm

import java.io.IOException;

public class TestaJavaIO {

	public static void main(String[] args) throws IOException {
		
		Escrita e = new Escrita();
		Leitura l = new Leitura();
		
		e.escreveOutputStream("Teste de Output Stream.");

		e.escrevePrintStream("Teste de Print Stream.");
		
		e.escreveFileWriter("Teste de File Writer");
		
		System.out.println("Conteúdo de arquivo-output-stream.txt:");
		l.leInputStreamArquivo("arquivo-output-stream.txt");
		
		System.out.println();
		
		System.out.println("Conteúdo de arquivo-print-stream.txt:");
		l.leScannerArquivo("arquivo-print-stream.txt");
		
		System.out.println();
		
		System.out.println("Conteúdo de arquivo-file-writer.txt:");
		l.leFileReader("arquivo-file-writer.txt");
		
		System.out.println();
		
		/*
		 * É possível tratar mais de uma exceção quase que ao mesmo tempo:
		 */
		System.out.println("Várias excecoes:");
		try {
			l.excecoes(-10);
		} catch (ArithmeticException excecao) {
			System.out.println("Erro de divisão por zero: " + excecao);
		} catch (NullPointerException excecao) {
			System.out.println("Erro de Null Pointer: " + excecao);
		} catch (IllegalArgumentException excecao) {
			System.out.println(excecao.getMessage());
		} catch (NumeroNegativoException excecao) {
			System.out.println(excecao.getMessage());
		} finally {
			/*
			 * Os blocos try e catch podem conter uma terceira cláusula
			 * chamada finally que indica o que deve ser feito após o 
			 * término do bloco try ou de um catch qualquer.
			 * 
			 * É interessante colocar algo que é imprescindível de ser 
			 * executado, caso o que você queria fazer tenha dado certo, 
			 * ou não. O caso mais comum é o de liberar um recurso no 
			 * finally, como um arquivo ou conexão com banco de dados, 
			 * para que possamos ter a certeza de que aquele arquivo 
			 * (ou conexão) vá ser fechado, mesmo que algo tenha falhado 
			 * no decorrer do código.
			 */
			System.out.println("Fim do bloco try-catch!");
		}
	}

}
