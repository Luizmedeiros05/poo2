import java.io.FileNotFoundException;

/*
 * Checked Exceptions
 * ------- ----------
 * 
 * Fica claro, com os programas Excecoes1, Excecoes2 e Excecoes3, que 
 * não é necessário declarar que você está tentando fazer algo onde um 
 * erro possa ocorrer. Os exemplos em Excecoes1, com ou sem o try/catch, 
 * compilaram e rodaram. Em um, o erro terminou o programa e, no outro, 
 * foi possível tratá-lo.
 * 
 * Mas não é só esse tipo de exceção que existe em Java. Um outro tipo, 
 * obriga a quem chama o método ou construtor a tratar essa exceção. 
 * Chamamos esse tipo de exceção de checked, pois o compilador checará 
 * se ela está sendo devidamente tratada, diferente das anteriores, 
 * conhecidas como unchecked.
 * 
 */
public class TestaArquivo {
	public static void metodo() throws java.io.FileNotFoundException {
		System.out.println("Início do método!");
		new java.io.FileInputStream("arquivo.txt");
		System.out.println("Fim do método!");
    }
	
	public static void main(String args[]) {
		TestaArquivo.metodo();
	}
}
/*
 * O código acima não compila e o compilador avisa que é necessário 
 * tratar o FileNotFoundException que pode ocorrer.
 * 
 * Para compilar e fazer o programa funcionar, temos duas maneiras 
 * que podemos tratar o problema. A primeira, é tratá-lo com o try 
 * e catch da mesma forma que usamos no exemplo Excecoes1:
 * 
   try {
        new java.io.FileInputStream("arquivo.txt");
    } catch (java.io.FileNotFoundException e) {
        System.out.println("Nao foi possível abrir o arquivo para leitura");
	}
 *
 * A segunda forma de tratar esse erro, é delegar ele para quem chamou o 
 * nosso método, isto é, passar para a frente.
 * 
   public static void metodo() throws java.io.FileNotFoundException {
    new java.io.FileInputStream("arquivo.txt");
   }
 * 
 * No início, existe uma grande tentação de sempre passar o problema pra 
 * frente para outros o tratarem. Pode ser que faça sentido, dependendo do 
 * caso, mas não até o main, por exemplo. Acontece que quem tenta abrir um 
 * arquivo sabe como lidar com um problema na leitura. Quem chamou um método 
 * no começo do programa pode não saber ou, pior ainda, tentar abrir cinco 
 * arquivos diferentes e não saber qual deles teve um problema!
 * 
 * Não há uma regra para decidir em que momento do seu programa você vai 
 * tratar determinada exceção. Isso vai depender de em que ponto você tem 
 * condições de tomar uma decisão em relação àquele erro. Enquanto não for 
 * o momento, você provavelmente vai preferir delegar a responsabilidade para 
 * o método que te invocou.
 */