/*
 * Exceção
 * -------
 * 
 * Uma exceção representa uma situação que normalmente não ocorre 
 * e representa algo de estranho ou inesperado no sistema.
 * 
 */

public class TestaDivisao {
	public static void main(String args[]) {
        int i = 5571;
        i = i / 0;
        System.out.println("O resultado  " + i);
    }
}
