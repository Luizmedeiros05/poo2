/*
 * Exceção
 * -------
 * 
 * Uma exceção representa uma situação que normalmente não ocorre 
 * e representa algo de estranho ou inesperado no sistema.
 * 
 */

public class TestaErro {
	public static void main(String[] args) {
        System.out.println("inicio do main");
        //try {
        	metodo1();
        //} catch (ArrayIndexOutOfBoundsException e) {
        //	System.out.println("erro: " + e);
        //}
        System.out.println("fim do main");
	}
	
	static void metodo1() {
        System.out.println("inicio do metodo1");
        //try {
        	metodo2();
        //} catch (ArrayIndexOutOfBoundsException e) {
        //	System.out.println("erro: " + e);
        //}
        System.out.println("fim do metodo1");
	}
	
    static void metodo2() {
        System.out.println("inicio do metodo2");
        int[] array = new int[10];
        
        //try {
        	for (int i = 0; i <= 15; i++) {
        		//try {
        			array[i] = i;
        			System.out.println(i);
        		//} catch (ArrayIndexOutOfBoundsException e) {
        		//	System.out.println("erro: " + e);
        		//}
        	}
        //} catch (ArrayIndexOutOfBoundsException e) {
        //	System.out.println("erro: " + e);
        //}
        System.out.println("fim do metodo2");
    }
}
/*
 * O método main chama metodo1 e esse, por sua vez, chama o metodo2. 
 * Cada um desses métodos pode ter suas próprias variáveis locais, 
 * isto é: o metodo1 não enxerga as variáveis declaradas dentro do 
 * main e por aí em diante.
 * 
 * Como o Java (e muitas das outras linguagens) faz isso? Toda invocação 
 * de método é empilhada em uma estrutura de dados que isola a área de 
 * memória de cada um. Quando um método termina (retorna), ele volta 
 * para o método que o invocou. Ele descobre isso através da pilha de 
 * execução (stack): basta remover o marcador que está no topo da pilha:
 * 
 * |		 |
 * |metodo2	 | <---|
 * |metodo1	 | <-|-|
 * |main	 | --|
 *  - - - - -
 * 
 * Porém, o nosso metodo2 propositalmente possui um enorme problema: 
 * está acessando um índice de array indevido para esse caso; o índice 
 * estará fora dos limites da array quando chegar em 10!
 * 
 * Execute o programa e veja o que acontece.
 * 
 * Essa é o conhecido rastro da pilha (stacktrace). É uma saída muito 
 * importante para o programador - tanto que, em qualquer fórum ou lista 
 * de discussão, é comum os programadores enviarem, juntamente com a 
 * descrição do problema, essa stacktrace.
 * 
 * O sistema de exceções do Java funciona da seguinte maneira: quando uma 
 * exceção é lançada (throw), a JVM entra em estado de alerta e verifica se
 * o método atual toma alguma precaução ao tentar executar esse trecho de 
 * código. Como podemos ver, o metodo2 não toma nenhuma medida diferente 
 * do que vimos até agora.
 * 
 * Como o metodo2 não está tratando esse problema, a JVM pára a execução 
 * dele anormalmente, sem esperar ele terminar, e volta um stackframe para 
 * a posição anterior da pilha, onde será feita nova verificação: 
 * “o metodo1 está se precavendo de um problema chamado 
 * ArrayIndexOutOfBoundsException?” “Não...” Volta 
 * para o main, onde também não há proteção, então a JVM morre (na verdade, 
 * quem morre é apenas a Thread corrente).
 * 
 * Obviamente, aqui estamos forçando esse caso e não faria sentido tomarmos 
 * cuidado com ele. É fácil arrumar um problema desses: basta percorrermos 
 * o array no máximo até o seu length.
 * 
 * Porém, apenas para entender o controle de fluxo de uma Exception, vamos 
 * colocar o código que vai tentar (try) executar o bloco perigoso e, caso 
 * o problema seja do tipo ArrayIndexOutOfBoundsException, ele será pego 
 * (caught). Repare que é interessante que cada exceção no Java tenha um 
 * tipo... ela pode ter atributos e métodos.
 * 
 * Remova os cometários das linhas 35, 44, 45 e 46 e execute novamente o
 * programa.
 * 
 * Agora comente novamente as linhas 35, 44, 45 e 46 e retire os 
 * comentários das linhas 37, 40, 41 e 42.
 * 
 * Qual a diferença?
 * 
 * Agora comente novamente as linhas 37, 40, 41 e 42  e retire os da  
 * chamada do metodo2.
 * 
 * Comente novamente as linhas do try/catch da chamada do metodo2 e retire
 * os comentários do try/catch da chamada do metodo1.
 *  
 * Repare que, a partir do momento que uma exception foi catched (pega, 
 * tratada, handled), a execução volta ao normal a partir daquele ponto.
 * 
 * Execute os programas Excecoes2, Excecoes3 e Excexoes4.
 * 
 * Em todos os casos, tais problemas provavelmente poderiam ser evitados 
 * pelo programador. É por esse motivo que o java não te obriga a usar o 
 * try/catch nessas exceptions e chamamos essas exceções de unchecked. 
 * Em outras palavras, o compilador não checa se você está tratando essas 
 * exceções.
 */
