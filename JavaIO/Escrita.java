import java.io.*;

public class Escrita {
	
	public void escreveOutputStream(String texto) throws IOException {
		/*
		 * O processo de escrita usa o OutputStream e é o mesmo do processo
		 * de leitura.
		 */
		OutputStream os = new FileOutputStream("arquivo-output-stream.txt");
		OutputStreamWriter osw = new OutputStreamWriter(os);
		BufferedWriter bw = new BufferedWriter(osw);
		
		bw.write(texto);
		/*
		 * O método write do BufferedWriter não insere o(s) caractere(s) de 
		 * quebra de linha. Para isso, você pode chamar o método newLine.
		 */
		bw.newLine();
		
		bw.close();
	}
	
	public void escrevePrintStream(String texto) throws IOException {
		
		/*
		 * A partir do Java 5, temos a classe java.util.Scanner, que facilita 
		 * bastante o trabalho de ler de um InputStream. Além disso, a classe 
		 * PrintStream possui um construtor que já recebe o nome de um arquivo 
		 * como argumento.
		 */
		PrintStream ps = new PrintStream("arquivo-print-stream.txt");
		ps.println(texto);
		ps.close();
	}
	
	public void escreveFileWriter(String texto) throws IOException {
		
		/*
		 * Existem duas classes chamadas java.io.FileReader e java.io.FileWriter. 
		 * Elas são atalhos para a leitura e escrita de arquivos.
		 */
		File f = new File("arquivo-file-writer.txt");
		f.createNewFile();
		FileWriter writer = new FileWriter(f); 
		writer.write(texto); 
		writer.flush();
		writer.close();
	}
}
