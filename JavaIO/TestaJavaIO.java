import java.io.IOException;

/*
 * Assim como todo o resto das bibliotecas em Java, a parte de controle 
 * de entrada e saída de dados (conhecido como io) é orientada a 
 * objetos e usa os principais conceitos mostrados até agora: interfaces, 
 * classes abstratas e polimorfismo.
 * 
 * A ideia atrás do polimorfismo no pacote java.io é de utilizar fluxos 
 * de entrada (InputStream) e de saída (OutputStream) para toda e 
 * qualquer operação, seja ela relativa a um arquivo, a um campo blob do 
 * banco de dados, a uma conexão remota via sockets, ou até mesmo às 
 * entrada e saída padrão de um programa (normalmente o teclado e o 
 * console).
 * 
 * As classes abstratas InputStream e OutputStream definem, 
 * respectivamente, o comportamento padrão dos fluxos em Java: em um 
 * fluxo de entrada, é possível ler bytes e, no fluxo de saída, escrever 
 * bytes.
 * 
 * A grande vantagem dessa abstração pode ser mostrada em um método 
 * qualquer que utiliza um OutputStream recebido como argumento para 
 * escrever em um fluxo de saída. Para onde o método está escrevendo? 
 * Não se sabe e não importa: quando o sistema precisar escrever em um 
 * arquivo ou em uma socket, basta chamar o mesmo método, já que ele 
 * aceita qualquer filha de OutputStream!
 */

public class TestaJavaIO {

	public static void main(String[] args) throws IOException {
		
		Escrita e = new Escrita();
		Leitura l = new Leitura();
		
		e.escreveOutputStream("Teste de Output Stream.");

		e.escrevePrintStream("Teste de Print Stream.");
		
		e.escreveFileWriter("Teste de File Writer");
		
		System.out.println("Conteúdo de arquivo-output-stream.txt:");
		l.leInputStreamArquivo("arquivo-output-stream.txt");
		
		System.out.println();
		
		System.out.println("Conteúdo de arquivo-print-stream.txt:");
		l.leScannerArquivo("arquivo-print-stream.txt");
		
		System.out.println();
		
		System.out.println("Conteúdo de arquivo-file-writer.txt:");
		l.leFileReader("arquivo-file-writer.txt");
	}

}
