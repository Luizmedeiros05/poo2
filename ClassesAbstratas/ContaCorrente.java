
public class ContaCorrente extends Conta{

	private double limite;
	
	@Override
	public boolean saque(double valor) {
		if (valor <= this.limite + this.saldo) {
			this.saldo -= valor;
			return true;
		} else {
			return false;
		}
	}
	
	public void setLimite(double valor) {
		this.limite = valor;
	}
	
	public double getLimite() {
		return this.limite;
	}
}
