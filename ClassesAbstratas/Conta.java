
public abstract class Conta {

	private static int totalDeContas;
	private int numero;
	private String nome;
	protected double saldo;
	
	/* 
	 * Classes Abstratas
	 * ------- ---------
	 * 
	 * Uma classe define as características e o comportamento de um 
	 * conjunto de objetos. Assim, os objetos são criados (instanciados) 
	 * a partir de classes.
	 * 
	 * Nem todas as classes são projetadas para permitir a criação de 
	 * objetos. Algumas classes são usadas apenas para agrupar 
	 * características comuns a diversas classes e, então, ser herdada 
	 * por outras classes. Tais classes são conhecidas como classes 
	 * abstratas.
	 * 
	 * Pode ser útil declarar classes – chamadas classes abstratas – 
	 * para as quais não se pretende criar objetos. Como elas só são 
	 * usadas como superclasses em hierarquias de herança, são chamadas 
	 * superclasses abstratas. Essas classes não podem ser usadas para 
	 * instanciar objetos, porque são incompletas. Suas subclasses devem 
	 * declarar as “partes ausentes” para tornarem-se classes concretas, 
	 * a partir das quais sê pode instanciar objetos.
	 * 
	 * As classes que não são abstratas são conhecidas como classes 
	 * concretas. As classes concretas podem ter instâncias diretas, ao 
	 * contrário das classes abstratas que só podem ter instâncias 
	 * indiretas, ou seja, apesar de a classe abstrata não poder ser 
	 * instanciada, ela deve ter subclasses concretas que por sua vez 
	 * podem ser instanciadas.
	 * 
	 * Por que criar uma classe que nunca será instanciada?
	 * Para organizar as características comuns às subclasses.
	 * 
	 * Para definir uma classe abstrata em Java, basta utilizar a palavra 
	 * reservada abstract. A palavra abstract deve ser inserida entre o 
	 * qualificador de acesso e o nome da classe.
	 * 
	 * Tendo em vista que uma classe abstrata não pode ser instanciada, 
	 * faz algum sentido uma classe abstrata ter construtores?
	 * 
	 * Sim! Como visto em herança, os construtores das subclasses se 
	 * utilizam dos construtores da superclasse. Assim, mesmo não podendo 
	 * ser instanciadas, é comum classes abstratas terem construtores que 
	 * inicializam seus próprios atributos e são utilizados pelas subclasses.
	 */
	
	public Conta() {
		Conta.totalDeContas = Conta.totalDeContas + 1;
	}
	
	public void deposito(double valor) {
		this.saldo = valor;
	}
	
	/*
	 * Métodos abstratos
	 * ------- ---------
	 * 
	 * Em algumas situações as classes abstratas podem ser utilizadas para 
	 * prover a definição de métodos que devem ser implementados em todas 
	 * as suas subclasses, sem apresentar uma implementação para esses métodos. 
	 * Tais métodos são chamados de métodos abstratos.
	 * 
	 * Toda classe que possui pelo menos um método abstrato é uma classe 
	 * abstrata, mas uma classe pode ser abstrata sem possuir nenhum método 
	 * abstrato.
	 * 
	 * Para definir um método abstrato em Java, utiliza-se a palavra reservada 
	 * abstract entre o especificador de visibilidade e o tipo de retorno do 
	 * método. Vale ressaltar que um método abstrato não tem corpo, ou seja, 
	 * apresenta apenas uma assinatura.
	 * 
	 * Para que uma subclasse de uma classe abstrata seja concreta, ela deve 
	 * obrigatoriamente apresentar implementações concretas para todos os 
	 * métodos abstratos de sua superclasse.
	 */
	public abstract boolean saque(double valor);
	
	public int getNumero() {
		return this.numero;
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public double getSaldo() {
		return this.saldo;
	}
	
	public static int getTotalDeContas() {
		return Conta.totalDeContas;
	}
	
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
}
