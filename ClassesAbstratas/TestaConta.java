
public class TestaConta {

	public static void main(String[] args) {
	
		ContaCorrente c1; 
		ContaPoupanca c2;

		c1 = new ContaCorrente();
		
		c1.setNumero(1);
		c1.setNome("José");
		c1.setLimite(500);
		c1.deposito(1000);
		
		System.out.println("Número da conta: " + c1.getNumero());
		System.out.println("Titular da conta: " + c1.getNome());
		System.out.println("Saldo da conta: " + c1.getSaldo());
		System.out.println("Limite da conta: " + c1.getLimite());
		System.out.println();
		System.out.println("O banco agora tem " + Conta.getTotalDeContas() + " conta.");
		System.out.println();
		
		c2 = new ContaPoupanca();
		
		c2.setNumero(2);
		c2.setNome("Maria");
		//c2.setLimite(500);
		c2.deposito(1000);
		
		System.out.println("Número da conta: " + c2.getNumero());
		System.out.println("Titular da conta: " + c2.getNome());
		System.out.println("Saldo da conta: " + c2.getSaldo());
		System.out.println();
		System.out.println("O banco agora tem " + Conta.getTotalDeContas() + " contas.");
	}

}
