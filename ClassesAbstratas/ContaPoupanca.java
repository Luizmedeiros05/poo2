
public class ContaPoupanca extends Conta{

	@Override
	public boolean saque(double valor) {
		if (this.getSaldo() >= valor) {
			this.saldo -= valor;
			return true;
		} else {
			return false;
		}
	}
}
