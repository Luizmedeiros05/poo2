/*
 * Polimorfismo
 * ------------
 * 
 * A palavra polimorfismo vem do grego "poli morfos" e significa a qualidade 
 * ou estado de ser capaz de assumir diferentes formas.
 * Na orientação a objetos, o polimorfismo é a capacidade de um objeto poder 
 * ser referenciado de várias formas.
 * 
 * O polimorfismo permite escrever programas que processam objetos que
 * compartilham a mesma superclasse (direta ou indiretamente) como se todos
 * fossem objetos da superclasse; isso pode simplificar a programação.
 * 
 * Em outras palavras, podemos ver o polimorfismo como a possibilidade de um
 * mesmo método ser executado de forma diferente de acordo com a classe do
 * objeto que aciona o método e com os parâmetros passados para o método.
 * 
 * Com o polimorfismo podemos projetar e implementar sistemas que são 
 * facilmente extensíveis – novas classes podem ser adicionadas com pouca ou
 * nenhuma alteração a partes gerais do programa, contanto que as novas 
 * classes façam parte da hierarquia de herança que o programa processa 
 * genericamente. As únicas partes de um programa que devem ser alteradas 
 * para acomodar as novas classes são aquelas que exigem conhecimento direto 
 * das novas classes que adicionamos à hierarquia.
 * 
 * O polimorfismo pode ser obtido pela utilização dos conceitos de herança,
 * sobrecarga de métodos e sobrescrita de método (também conhecida como
 * redefinição ou reescrita de método).
 * 
 * Sobrescrita
 * -----------
 * 
 * A técnica de sobrescrita permite reescrever um método em uma subclasse de
 * forma que tenha comportamento diferente do método de mesma assinatura
 * existente na sua superclasse.
 * 
 * Sobrecarga
 * ----------
 * 
 * Métodos de mesmo nome podem ser declarados na mesma classe, contanto
 * que tenham diferentes conjuntos de parâmetros (determinado pelo número,
 * tipos e ordem dos parâmetros). Isso é chamado sobrecarga de método.
 * 
 * Para que os métodos de mesmo nome possam ser diferenciados, eles devem
 * possuir assinaturas diferentes. A assinatura (signature) de um método é
 * composta pelo nome do método e por uma lista que indica os tipos de todos
 * os seus argumentos. Assim, métodos com mesmo nome são considerados
 * diferentes se recebem um diferente número de argumentos ou tipos 
 * diferentes de argumentos e têm, portanto, uma assinatura diferente.
 * 
 * Quando um método sobrecarregado é chamado, o compilador Java 
 * seleciona o método adequado examinando o número, os tipos e a ordem dos
 * argumentos na chamada.
 * 
 * 
 */
public class TestaEmpregadoDaFalculdade {

	public static void main(String[] args) {
	
		EmpregadoDaFaculdade empregado1 = new ProfessorDaFaculdade();
		EmpregadoDaFaculdade empregado2 = new EmpregadoDaFaculdade();
		EmpregadoDaFaculdade empregado3 = new Reitor();
		
		empregado1.setNome("Jose");
		empregado1.setSalario(1000);
		
		empregado2.setNome("Maria");
		empregado2.setSalario(1000);
		
		empregado3.setNome("Joao");
		empregado3.setSalario(2000);
		
		GeradorDeRelatorio relatorio = new GeradorDeRelatorio();
		relatorio.mostrarRelatorio(empregado1);
		relatorio.mostrarRelatorio(empregado2);
		relatorio.mostrarRelatorio(empregado3);
	}

}
