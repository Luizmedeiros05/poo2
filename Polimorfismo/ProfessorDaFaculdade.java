
public class ProfessorDaFaculdade extends EmpregadoDaFaculdade {
	private int horasDeAula = 10;
	
	@Override
	double getSalario() {
		return super.getSalario() + this.horasDeAula * 10;
	}
	
	@Override
	String getInfo() {
		String informacaoBasica = super.getInfo();
		String informacao = informacaoBasica + " horas de aula: "
				+ this.horasDeAula;
		return informacao;
	}
	
}
