Crie a classe Quadrilatero com os seguintes atributos:

- lado1
- lado2
- altura

E, pelo menos, os seguintes métodos:

- calculaArea

obs: área do quadrado: lado1 x lado1

Crie a classe Retângulo, herdando a classe Quadrilatero com o seguinte método:

- calculaArea

obs: área do retângulo: lado1 x lado2

Crie a classe Trapezio, herdando a classe Quadrilatero com o seguinte método:

- calculaArea

obs: área do trapézio: ((lado1 + lado2) x altura ) / 2

Crie uma classe de teste que calcula a área de um quadrado, a área de um 
retângulo e a área de um trapézio.
