
public class EmpregadoDaFaculdade {
	private String nome;
	private double salario;
	
	double getSalario() {
		return this.salario;
	}
	
	String getInfo() {
		return "nome: " + this.nome + " com salário " + this.salario;
	}
	
	void setNome(String nome) {
		this.nome = nome;
	}
	
	void setSalario(double salario) {
		this.salario = salario;
	}
}
