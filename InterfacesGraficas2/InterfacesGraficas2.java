import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class InterfacesGraficas2 extends JFrame implements ActionListener {

	JLabel label1, label2, label3, label4, label5;
	JTextField tadicionar, t1, t2, t3;
	JComboBox combo;
	JButton novoItem;
	JCheckBox c1;
	JRadioButton r1, r2;
	ButtonGroup radiogroup;
	
	public void criarComponentesJanela() {
		this.label1 = new JLabel("Animais");
		this.novoItem = new JButton("Adiciona Item");
		this.tadicionar = new JTextField();
		/* 
		 * A classe JComboBox é utilizada para criar caixas que permitem que o usuário
		 * selecione apenas um item da sua lista de opções.
		 * 
		 * Para criar um objeto do tipo JComboBox, é necessário passar um vetor de
		 * Strings que indicará as opções de seleção a serem exibidas na caixa.
		 * 
		 * Dentre os métodos mais utilizados da classe JComboBox, destacam-se:
		 * - getSelectedIndex: retorna o índice do item selecionado no combobox. É
		 * importante notar que o índice do primeiro elemento é 0.
		 * - removeItemAt: dado um índice, elimina do combobox o item que está
		 * nessa posição.
		 * - removeAllItens: remove todos os itens do combobox.
		 * - addItem: dado um novo item, insere-o no combobox.
		 * - getSelectedItem: retorna o item selecionado.
		 * - getItemCount: retorna a quantidade de itens do combobox.
		 */
		String[] animais = {"Leão", "Elefante", "Cobra", "Tartaruga"};
		combo = new JComboBox(animais);
		
		this.label2 = new JLabel("O rato roeu");
		this.label2.setFont(new Font("Arial", Font.PLAIN, 20));
		/*
		 * As caixas de opção são criadas a partir da classe JCheckBox 
		 * e permitem representar uma opção que está ativada (true) ou
		 *  desativada (false).
		 *  
		 *  Para verificar se um objeto JCheckBox está selecionado, 
		 *  utilizamos o método isSelected().
		 */
		this.c1 = new JCheckBox("Negrito");
		
		this.label3 = new JLabel("Número");
		this.label4 = new JLabel("Número");
		this.label5 = new JLabel("Resultado");
		this.t1 = new JTextField(5);
		this.t2 = new JTextField(5);
		this.t3 = new JTextField(5);
		this.t3.setEditable(false);
		
		/* 
		 * A classe JRadioButton é utilizada para criar “botões de rádio”.
		 * Esses botões são graficamente muito semelhantes aos checkbox. 
		 * 
		 * A diferença entre radiobutton e checkbox está no fato que os 
		 * primeiros são usados em conjuntos de forma que apenas um elemento 
		 * do conjunto pode estar selecionado em um dado momento. 
		 * 
		 * Mas, para haver esse controle, os JRadioButton devem estar em um 
		 * grupo representado pela classe ButtonGroup.
		 * 
		 * Um objeto da classe ButtonGroup não é visual, ou seja, não tem 
		 * impacto na interface gráfica, mas sem ele perdemos a garantia de 
		 * que apenas um botão de rádio está selecionado.
		 * 
		 * Dentre os métodos mais utilizados da classe JRadioButton, estão:
		 * - setMnemonic: permite que uma combinação de teclas tenha o mesmo
		 * efeito do clique sobre um objeto JRadioButton.
		 * - isSelected: determina se um botão de rádio está selecionado.
		 * - setSelected: recebe um parâmetro booleano (true ou false) que 
		 * faz com que o botão de rádio seja selecionado ou não.
		 */
		this.r1 = new JRadioButton("+");
		this.r2 = new JRadioButton("-");
		this.radiogroup = new ButtonGroup();
	}
	
	public void actionPerformed(ActionEvent e) {
		// Quando o botão for pressionado, adiciona ítem à lista
		if (e.getSource() == this.novoItem && this.tadicionar.getText().length() != 0) {
			this.combo.addItem(tadicionar.getText());
			this.tadicionar.setText("");
		}
		
		// Caso o checkbox seja selecionado, coloca o texto em negrito.
		// Caso seja desmarcado, o texto perde o negrito.
		if (e.getSource() == this.c1) {
			if (this.c1.isSelected())
				this.label2.setFont(new Font("Arial", Font.BOLD, 20));
			else
				this.label2.setFont(new Font("Arial", Font.PLAIN, 20));
		}

		if (e.getSource() == this.r1 || e.getSource() == this.r2) {
			float n1 = Float.parseFloat(t1.getText());
			float n2 = Float.parseFloat(t2.getText());
			float resultado = 0;
			// Quando o primeiro radiobutton for escolhido, soma os dois números
			if (e.getSource() == this.r1) resultado = n1 + n2;
			// Quando o segundo radiobutton for escolhido, subtrai o segundo do primeiro
			if (e.getSource() == this.r2) resultado = n1 - n2;
			// Coloca o resultado no campo de texto.
			t3.setText("" + resultado);
		}
	}
	
	public void setarAtributosJanela() {
		this.setTitle("Interfaces Graficas 2");
		this.setBounds(50, 50, 400, 300);
		this.getContentPane().setLayout(new GridLayout(7, 2));
		this.getContentPane().add(this.label1);
		this.getContentPane().add(this.combo);
		this.getContentPane().add(this.novoItem);
		this.getContentPane().add(this.tadicionar);
		this.getContentPane().add(this.label2);
		this.getContentPane().add(this.c1);
		this.getContentPane().add(this.label3);
		this.getContentPane().add(this.label4);
		this.getContentPane().add(this.t1);
		this.getContentPane().add(this.t2);
		this.getContentPane().add(this.r1);
		this.getContentPane().add(this.r2);
		this.getContentPane().add(this.label5);
		this.getContentPane().add(this.t3);
	}
	
	// Adiciona radiobuttons no buttongroup
	public void setarAtributosButtongroup() {
		this.radiogroup.add(this.r1);
		this.radiogroup.add(this.r1);
	}
	
	public void adicionarListeners() {
		this.novoItem.addActionListener(this);
		this.combo.addActionListener(this);
		this.c1.addActionListener(this);
		this.r1.addActionListener(this);
		this.r2.addActionListener(this);
	}
	
	public InterfacesGraficas2() {
		this.criarComponentesJanela();
		this.setarAtributosJanela();
		this.setarAtributosButtongroup();
		this.adicionarListeners();
	}

	public static void main(String[] args) {
		JFrame janela = new InterfacesGraficas2();
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		janela.setVisible(true);
	}
}
