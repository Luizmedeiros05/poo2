import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;

/*
 * O padrão de layout GridLayout organiza os objetos como uma tabela, 
 * com células de objetos de mesmo tamanho. É um layout flexível, 
 * pois uma vez redimensionada a janela ele ajusta automaticamente os 
 * objetos de forma que o padrão se mantenha, ou seja, que cada objeto 
 * de cada célula seja apresentado com o mesmo tamanho.
 */
public class ExemploGridLayout extends JFrame{

	public ExemploGridLayout() {
		// Instancia GridLayout em layout com 2 linhas, 3 colunas e 
		// 10 pixels de espaço entre as células na horizonta e na vertical
		GridLayout layout = new GridLayout(2, 3, 10, 10);
		// Habilita o layout
		this.setLayout(layout);
		// Adiciona labels
		this.add(new JLabel("Batatinha quando nasce espalha a rama pelo chão."));
		this.add(new JLabel("Menininha quando dorme põe a mão no coração."));
		this.add(new JLabel("Sou pequenininha do tamanho de um botão, "));
		this.add(new JLabel("carrego papai no bolso e mamãe no coração."));
		this.add(new JLabel("O bolso furou e o papai caiu no chão."));
		this.add(new JLabel("Mamãe que é mais querida ficou no coração."));
	}
	
	public static void main(String[] args) {
		// Instancia janela
		JFrame janela = new ExemploGridLayout();
		// Define tamanho e posição da janela (x1, y1, x2, y2)
		janela.setBounds(200, 50, 1150, 200);
		janela.setTitle("GridLayout");
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		janela.setVisible(true);
	}

}
