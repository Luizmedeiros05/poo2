import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;

/* 
 * O padrão de layout FlowLayout insere os objetos, um após o outro, 
 * linha a linha. Assim, quando não é mais possível inserir objetos 
 * numa linha, é criada uma nova linha e novos objetos podem ser inseridos. 
 * Esse padrão de layout assemelha-se a um editor de textos, pois quando 
 * não existe mais espaço numa dada linha é criada uma nova linha para 
 * inserção de mais conteúdos.
 */
public class ExemploFlowLayout extends JFrame{

	public ExemploFlowLayout() {
		// Instancia FlowLayout com alinhamento centralizado e espaçamento
		// de 10 pixels na horizontal e na vertical entre os objetos inseridos.
		FlowLayout layout = new FlowLayout(FlowLayout.CENTER, 10, 10);
		// Habilita o layout
		this.setLayout(layout);
		// Adiciona labels
		this.add(new JLabel("Batatinha quando nasce espalha a rama pelo chão."));
		this.add(new JLabel("Menininha quando dorme põe a mão no coração."));
		this.add(new JLabel("Sou pequenininha do tamanho de um botão, "));
		this.add(new JLabel("carrego papai no bolso e mamãe no coração"));
		this.add(new JLabel("O bolso furou e o papai caiu no chão."));
		this.add(new JLabel("Mamãe que é mais querida ficou no coração."));
	}
	
	public static void main(String[] args) {
		
		JFrame janela = new ExemploFlowLayout();
		janela.setBounds(250, 50, 400, 200);
		janela.setTitle("FlowLayout");
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		janela.setVisible(true);
	}

}
