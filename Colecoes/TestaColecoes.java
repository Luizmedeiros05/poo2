import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;


/*
 * Collections
 * -----------
 * 
 * A API do Collections é robusta e possui diversas classes que 
 * representam estruturas de dados avançadas.
 * 
 * Por exemplo, não é necessário reinventar a roda e criar uma lista 
 * ligada, mas sim utilizar aquela que o Java disponibiliza.
 * 
 */
public class TestaColecoes {

	public static void main(String[] args) {

		/*
		 * Listas
		 * ------
		 * 
		 * A estrutura de dados lista é utilizada para armazenar conjuntos de 
		 * elementos. A vantagem em utilizar listas em lugar de vetores é o 
		 * fato de as listas serem alocadas dinamicamente de forma que não 
		 * precisamos prever seu tamanho máximo. Java fornece classes que 
		 * implementam o conceito de lista. A classe que será vista aqui 
		 * será a ArrayList. Para trabalharmos com a classe ArrayList 
		 * precisamos conhecer seus métodos, listados a seguir:
		 * 
		 * - public ArrayList(): cria um ArrayList vazio.
		 * - public boolean add(<elemento>): adiciona um elemento no  final 
		 * da lista. 
		 * - public void add(index, <elemento>): adiciona um elemento na 
		 * posição index.
		 * - public <elemento> get(int index): obtém o elemento de índice index. 
		 * - public <elemento> remove(int index): retorna o elemento de índice 
		 * index e o elimina da lista.
		 * - public boolean isEmpty(): verifica se a lista está vazia.
		 * - public boolean contains(Object o): indica se o elemento existe na lista.
		 * 
		 * Para navegarmos em uma lista, utilizaremos a interface Iterator. 
		 * Seguem os principais métodos de Iterator:
		 * 
		 * - boolean hasNext(): verifica se existe próximo elemento na lista;
		 * - next(): obtém o elemento sob o Iterator e avança para o próximo 
		 * elemento;
		 * - void remove(): remove o elemento sob o Iterator.
		 * 
		 * O ArrayList pode armazenar objetos de quaisquer tipo. Assim, quando 
		 * obtemos um objeto ele retorna uma instância do tipo Object. Por isso
		 * é necessário fazer a conversão explícita para o tipo desejado no retorno.
		 * 
		 * ArrayList não é um Array!
		 * 
		 * É comum confundir uma ArrayList com um array, porém ela não é um array. 
		 * O que ocorre é que, internamente, ela usa um array como estrutura para 
		 * armazenar os dados, porém este atributo está propriamente encapsulado 
		 * e nã há como acessá-lo. Repare, também, que nã é possível usar [] com 
		 * uma ArrayList, nem acessar atributo length. Não há relação!
		 */
		
		/*
		 * Em qualquer lista, é possível colocar qualquer Object. Com 
		 * isso, é possível misturar objetos.
		 * 
		 * Mas e depois, na hora de recuperar esses objetos? Como o 
		 * método get devolve um Object, precisamos fazer o cast. Mas 
		 * com uma lista com vários objetos de tipos diferentes, isso 
		 * pode não ser tão simples.
		 * 
		 * Geralmente, não nos interessa uma lista com vários tipos de 
		 * objetos misturados. No Java 5.0, podemos usar o recurso de 
		 * Generics para restringir as listas a um determinado tipo de 
		 * objetos (e não qualquer Object).
		 */
		List<Conta> lista = new ArrayList();
		
		Conta c = new ContaCorrente(1, "José", 100);
		lista.add(c);
		
		//c = new ContaPoupanca(2,"João");
		//lista.add(0,c); 
		
		c = new ContaCorrente(2,"João", 50);
		lista.add(c);
		
		c = new ContaCorrente(3,"Maria", 200);
		lista.add(c);
		
		ContaPoupanca p = new ContaPoupanca(3,"Maria");
		p.saldo = 200;
		//lista.add(p);
		int x = c.compareTo(p);
		System.out.println("x: " + x);
		
		Random z = new Random();
		int w = z.nextInt((120-97) + 1) + 97;
		System.out.println(w);
		
		Iterator i = lista.iterator();
		while(i.hasNext()) {
			c = (Conta) i.next();
			System.out.println("Lista: Numero da Conta: " + c.getNumero());
			System.out.println("Lista: Nome na Conta: " + c.getNome());
		}
		System.out.println();
		/*
		 * Acesso aleatório e percorrendo listas com get
		 * ------ --------- - ----------- ------ --- ---
		 * 
		 * Algumas listas, como a ArrayList, têm acesso aleatório aos 
		 * seus elementos: a busca por um elemento em uma determinada 
		 * posição é feita de maneira imediata, sem que a lista inteira 
		 * seja percorrida (que chamamos de acesso sequencial).
		 * 
		 * Neste caso, o acesso através do método get(int) é muito rápido. 
		 * Caso contrário, percorrer uma lista usando um for pode ser 
		 * desastroso. Ao percorrermos uma lista, devemos usar sempre um 
		 * Iterator ou enhanced for.
		 */
		
		/*
		 * Ordenação: Collections.sort
		 * 
		 * Vimos anteriormente que as listas são percorridas de maneira 
		 * pré-determinada de acordo com a inclusão dos itens. Mas, muitas 
		 * vezes, queremos percorrer a nossa lista de maneira ordenada.
		 * 
		 * A classe Collections traz um método estático sort que recebe um 
		 * List como argumento e o ordena por ordem crescente.
		 */
		
		Collections.sort(lista);
		
		i = lista.iterator();
		while(i.hasNext()) {
			c = (Conta) i.next();
			System.out.println("Lista: Numero da Conta: " + c.getNumero());
			System.out.println("Lista: Nome na Conta: " + c.getNome());
		}
	}

	/*
	 * A classe Collections traz uma grande quantidade de métodos 
	 * estáticos úteis na manipulação de coleções.
	 * 
	 * - binarySearch(List, Object): Realiza uma busca binária por 
	 * determinado elemento na lista ordenada e retorna sua posição 
	 * ou um número negativo, caso não encontrado.
	 * 
	 * - max(Collection): Retorna o maior elemento da coleção.
	 * 
	 * - min(Collection): Retorna o menor elemento da coleção.
	 * 
	 * - reverse(List): Inverte a lista.
	 */
}