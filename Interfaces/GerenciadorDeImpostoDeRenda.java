/*
 * Repare que, de dentro do GerenciadorDeImpostoDeRenda, nã é possível 
 * acessar o método getSaldo, por exemplo, pois há garantia de que o 
 * Tributavel que vai ser passado como argumento tem esse método. A 
 * única certeza é de que esse objeto tem os métodos declarados na 
 * interface Tributavel.
 */
public class GerenciadorDeImpostoDeRenda {
	
	private double total;
	
    void adiciona(Tributavel t) {
        System.out.println("Adicionando tributavel: " + t);
        this.total +=  t.calculaTributos();
    }
    
    public double getTotal() {
        return this.total;
    }
}