
public class ContaCorrente extends Conta implements Tributavel{

	private double limite;
	
	public ContaCorrente() {
		
	}
	
	public ContaCorrente(int numero, String nome, double limite) {
		this.numero = numero;
		this.nome = nome;
		this.limite = limite;
		this.tipo = "Conta Corrente";
	}
	
    public double calculaTributos() {
        return this.getSaldo() * 0.01;
    }
	
	public boolean saque(double valor) {
		if (valor <= this.limite + this.saldo) {
			this.saldo -= valor;
			return true;
		} else {
			return false;
		}
	}
	
	public void setLimite(double valor) {
		this.limite = valor;
	}
	
	public double getLimite() {
		return this.limite;
	}
}
