
public class ContaPoupanca extends Conta {
	
	public ContaPoupanca(int numero, String nome) {
		this.numero = numero;
		this.nome = nome;
		this.tipo = "Conta Poupoança";
	}
	
	public boolean saque(double valor) {
		if (this.getSaldo() >= valor) {
			this.saldo -= valor;
			return true;
		} else {
			return false;
		}
	}
}
