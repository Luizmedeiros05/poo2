/*
 * Interfaces
 * ----------
 * 
 * Uma Interface é uma coleção de métodos que indica que uma classe 
 * possui algum comportamento além do que herda de suas superclasses.
 *  
 * Os métodos incluídos em uma interface não definem esse comportamento; 
 * essa tarefa é deixada para as classes que implementam a interface.
 * 
 * Uma interface pode ser vista como um conjunto de declarações de 
 * métodos, sem as respectivas implementações.
 * 
 * Uma interface é parecida com uma classe; porém, em uma interface, 
 * todos os métodos são públicos e abstratos e todos os atributos são 
 * públicos, estáticos e constantes.
 * 
 * Em resumo, uma interface pode definir uma série de métodos, mas nunca 
 * conter implementação deles. Ela só expõe o que o objeto deve fazer, e 
 * não como ele faz, nem o que ele tem. Como ele faz vai ser definido em 
 * uma implementação dessa interface.
 * 
 * A sintaxe para criar uma interface é muito parecida com a sintaxe 
 * para criar uma classe: public interface <nome_da_interface>.
 */
public interface Tributavel {
	double calculaTributos();
}
/*
 * O uso de interfaces é recomendável no desenvolvimento de sistemas 
 * para fornecer um contexto menos acoplado e mais simplificado de 
 * programação. Vamos supor, por exemplo, que temos uma interface 
 * responsável pela comunicação com banco de dados; dessa forma, 
 * qualquer classe que implementar a interface responderá a todas as 
 * funcionalidades para acesso a banco. Suponhamos que um novo banco 
 * seja elaborado e que desejemos fazer a troca do banco antigo por 
 * esse banco novo; será necessário apenas elaborar a classe que 
 * implemente a interface de acesso a esse banco novo e, ao invés 
 * de utilizarmos um objeto da classe antiga, utilizaremos um objeto 
 * da nova classe elaborada.
 */ 
