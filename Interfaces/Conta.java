
public abstract class Conta {


	protected static int totalDeContas;
	protected int numero;
	protected String nome;
	protected double saldo;
	protected String tipo;
	

	public Conta() {
		Conta.totalDeContas = Conta.totalDeContas + 1;
	}
	
	public void deposito(double valor) {
		this.saldo = valor;
	}
	
	public int getNumero() {
		return this.numero;
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public double getSaldo() {
		return this.saldo;
	}

	public String getTipo() {
		return this.tipo;
	}

	public static int getTotalDeContas() {
		return Conta.totalDeContas;
	}
}
